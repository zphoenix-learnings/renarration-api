# Renarration API

Renarration API is an Express application based on NodeJS thats the backend system of Renarration.

## Setup

1. Install docker and docker-compose.
2. Clone this repository.

## Upgrades

This step is to be followed for all upgrades after first time step

1. ```docker-compose down ```
2. ``` docker-compose pull ```
3. ``` docker-compose up -d ```

## Running

```bash
$ docker-compose pull
$ docker-compose up -d
```

