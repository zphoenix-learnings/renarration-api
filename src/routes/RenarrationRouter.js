import { createRenarration, getAllRenarrations, getRenarrationById, getRenarrationsByUrl, updateRenarration, verifySharing } from "../controllers/Renarration.js";


  
  export default function (fastify, options, done) {
    fastify.post('/create', createRenarration);
    fastify.get('/', getAllRenarrations);
    fastify.post('/verifyRenarration', verifySharing);
    fastify.get('/:id', getRenarrationById);
    fastify.post('/by-url', getRenarrationsByUrl);
    fastify.put('/update/:id', updateRenarration);
    done();
  }
  