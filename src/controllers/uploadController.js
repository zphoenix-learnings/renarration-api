
import dotenv from 'dotenv';
import * as Minio from 'minio'

// Load environment variables from a .env file if available
dotenv.config();

const minioClient = new Minio.Client({
  endPoint: process.env.MINIO_S3_ENDPOINT_URL,
  useSSL: process.env.MINIO_USE_SSL === 'true' || false,
  accessKey: process.env.MINIO_ACCESS_KEY,
  secretKey: process.env.MINIO_SECRET_KEY,
});

const uploadFileController = async (request, reply) => {
  const data = await request.file();
  if (!data.file) {
    return reply.code(400).send('No file uploaded.');
  }

  try {
    const fileData = await data.toBuffer();
    const fileName = data.filename;
    const bucketName = process.env.MINIO_BUCKET_NAME ;

    // Check if the bucket exists, if not, create it
    const bucketExists = await minioClient.bucketExists(bucketName);
    if (!bucketExists) {
      await minioClient.makeBucket(bucketName, 'us-east-1');
    }

    // Upload the file to MinIO
    await minioClient.putObject(bucketName, fileName, fileData);

    // Generate a URL for the uploaded file
    const fileUrl = `https://${process.env.MINIO_S3_ENDPOINT_URL }/${bucketName}/${fileName}`;
    reply.send(fileUrl);
  } catch (error) {
    console.error('Error uploading file:', error);
    reply.code(500).send('Error uploading file', error.message);
  }
};

export default uploadFileController;
