import mongoose from "mongoose";

// Define Sweet schema
const RenarrationSchema = new mongoose.Schema({
    renarrationUrl: {
        type: String,
    },
    renarrationTitle: {
        type: String,
    },
    sweetcount:{
type: Number
    },
    sweets: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Sweet' }],
    sharingId: {
        type: String,
        required: true
    },
    
});

// Define Sweet model
export const Renarration = mongoose.model('Renarration', RenarrationSchema);